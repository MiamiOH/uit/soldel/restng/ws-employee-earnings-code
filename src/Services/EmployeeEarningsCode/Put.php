<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/4/18
 * Time: 4:40 PM
 */

namespace MiamiOH\WSEmployeeEarningsCode\Services\EmployeeEarningsCode;

use MiamiOH\RESTng\Util\User;
use MiamiOH\WSEmployeeEarningsCode\Objects\EmployeeEarningsCode;
use MiamiOH\WSEmployeeEarningsCode\Repositories\EmployeeEarningsCodeRepository;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;

class Put
{
    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     * @param EmployeeEarningsCodeRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function putSingle(
        Request $request,
        Response $response,
        User $user,
        EmployeeEarningsCodeRepository $repository
    ): Response {
        $status = \MiamiOH\RESTng\App::API_CREATED;

        $data = $request->getData();

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayload($payload);
            $response->setStatus($status);
            return $response;
        }

        $data['userId'] = strtoupper($user->getUsername());
        $data['dataOrigin'] = 'WebService';

        try {
            $employeeEarningsCode = EmployeeEarningsCode::fromArray($data);
            $result = $repository->update($employeeEarningsCode);
        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        // DONE
        $response->setPayload(['1 record updated or created.']);
        $response->setStatus($status);
        return $response;
    }
}
