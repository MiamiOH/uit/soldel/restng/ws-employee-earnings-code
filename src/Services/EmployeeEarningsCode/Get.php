<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 9/26/18
 * Time: 10:39 AM
 */

namespace MiamiOH\WSEmployeeEarningsCode\Services\EmployeeEarningsCode;

use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;
use MiamiOH\WSEmployeeEarningsCode\Repositories\EmployeeEarningsCodeRepository;

class Get
{
    private $rules = [
        'muid' => ['required', 'regex:/^(\w{1,8}|\d{1,8}|\+[0-9]{1,8})$/'],
        'positionNumber' => ['regex:/^\d{1,6}$/'],
        'positionSuffix' => ['regex:/^\d{1,2}$/'],
        'effectiveDate' => ['date'],
        'earnCode' => ['max:3'],
        'active' => ['regex:/^Y|N$/i']
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param EmployeeEarningsCodeRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function getSingle(
        Request $request,
        Response $response,
        EmployeeEarningsCodeRepository $repository
    ): Response {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $muid = $options['muid'] ?? '';
        $positionNumber = $options['positionNumber'] ?? '';
        $positionSuffix = $options['positionSuffix'] ?? '';
        $effectiveDate = $options['effectiveDate'] ?? '';
        $earnCode = $options['earnCode'] ?? '';
        $active = $options['active'] ?? '';

        // Validate id
        $muid = strtoupper(trim($muid));
        $positionSuffix = trim($positionSuffix);
        $positionNumber = trim($positionNumber);
        $effectiveDate = trim($effectiveDate);
        $earnCode = trim($earnCode);
        $active = strtoupper(trim($active));

        $validator = RESTngValidatorFactory::make(
            [
                'muid' => $muid,
                'positionNumber' => $positionNumber,
                'positionSuffix' => $positionSuffix,
                'effectiveDate' => $effectiveDate,
                'earnCode' => $earnCode,
                'active' => $active
            ],
            $this->rules,
            [
                'active.regex' => "Active indicator must be Y or N.",
            ]
        );

        if ($validator->fails()) {
            $response->setPayload([
                $validator->errors()->getMessages()
            ]);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);

            return $response;
        }


        $payload = $repository->read($muid, $positionNumber, $positionSuffix, $effectiveDate, $earnCode, $active);

        // DONE
        $response->setTotalObjects(count($payload));
        $response->setPayload($payload);
        $response->setStatus($status);
        return $response;
    }
}
