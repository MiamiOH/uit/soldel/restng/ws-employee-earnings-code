<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 9/24/18
 * Time: 3:05 PM
 */

namespace MiamiOH\WSEmployeeEarningsCode\Services\EmployeeEarningsCode;

use MiamiOH\WSEmployeeEarningsCode\DataTransformers\EmployeeEarningsCodeDataTransformer;
use MiamiOH\WSEmployeeEarningsCode\Repositories\EmployeeEarningsCodeRepository;
use MiamiOH\WSEmployeeEarningsCode\Repositories\EmployeeEarningsCodeRepositorySQL;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\WSEmployeeEarningsCode\Repositories\MUIDRepositoryResourceCall;
use MiamiOH\WSEmployeeEarningsCode\Utilities\DataTransformer;

class Service extends \MiamiOH\RESTng\Service
{
    /**
     * @var string
     */
    protected $dataSource = 'MUWS_GEN_PROD';

    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    private $user = null;

    /**
     * @var EmployeeEarningsCodeRepository
     */
    private $repository = null;


    public function getDependencies()
    {
        date_default_timezone_set('US/Eastern');

        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();

        $muidCall = function (array $params = [], array $options = []) {
            return $this->callResource('muid.get', $params, $options);
        };

        $muidRepository = new MUIDRepositoryResourceCall($muidCall);

        $this->repository = new EmployeeEarningsCodeRepositorySQL(
            $muidRepository,
            new EmployeeEarningsCodeDataTransformer()
        );
    }

    /**
     * @throws \Exception
     */
    public function postSingle()
    {
        $this->getDependencies();

        $postService = new Post();

        $response = $postService->postSingle(
            $this->request,
            $this->response,
            $this->user,
            $this->repository
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function putSingle()
    {
        $this->getDependencies();

        $putService = new Put();

        $response = $putService->putSingle(
            $this->request,
            $this->response,
            $this->user,
            $this->repository
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function getSingle()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getSingle(
            $this->request,
            $this->response,
            $this->repository
        );

        return $response;
    }
}
