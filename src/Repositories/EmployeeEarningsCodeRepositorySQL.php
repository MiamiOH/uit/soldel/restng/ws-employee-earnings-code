<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/5/18
 * Time: 2:33 PM
 */

namespace MiamiOH\WSEmployeeEarningsCode\Repositories;

use MiamiOH\WSEmployeeEarningsCode\DataTransformers\DataTransformer;
use MiamiOH\WSEmployeeEarningsCode\EloquentModels\EmployeeEarningsCodeEloquentModel;
use MiamiOH\WSEmployeeEarningsCode\Objects\EmployeeEarningsCode;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Class EmployeeEarningsCodeRepositorySQL
 * @package MiamiOH\WSEmployeeEarningsCode\Repositories
 */
class EmployeeEarningsCodeRepositorySQL implements EmployeeEarningsCodeRepository
{
    /**
     * @var MUIDRepository
     */
    private $muidRepository;

    /**
     * @var DataTransformer
     */
    private $dataTransformer;

    public function __construct(MUIDRepository $muidRepository, DataTransformer $dataTransformer)
    {
        $this->muidRepository = $muidRepository;
        $this->dataTransformer = $dataTransformer;
    }

    /**
     * @param EmployeeEarningsCode $employeeEarningsCode
     * @return bool
     * @throws \Exception
     */
    public function create(
        EmployeeEarningsCode $employeeEarningsCode
    ): bool {
        $employeeId = $employeeEarningsCode->getMuid();
        $muid = $this->muidRepository->readMUID($employeeId);
        $employeePidm = $muid['pidm'];

        if (empty($employeePidm)) {
            throw new \Exception("Cannot find employee pidm for muid '$employeeId'.");
        }

        EmployeeEarningsCodeEloquentModel::create([
            'nbrearn_pidm' => $muid['pidm'],
            'nbrearn_shift' => $employeeEarningsCode->getShift(),
            'nbrearn_active_ind' => $employeeEarningsCode->getActive(),
            'nbrearn_earn_code' => $employeeEarningsCode->getEarnCode(),
            'nbrearn_effective_date' => $employeeEarningsCode->getEffectiveDate()->format('Y-m-d H:i:s'),
            'nbrearn_posn' => $employeeEarningsCode->getPositionNumber(),
            'nbrearn_suff' => $employeeEarningsCode->getPositionSuffix(),
            'nbrearn_special_rate' => $employeeEarningsCode->getSpecialRate(),
            'nbrearn_hrs' => $employeeEarningsCode->getHours(),
            'nbrearn_deemed_hrs' => $employeeEarningsCode->getDeemedHours(),
            'nbrearn_user_id' => $employeeEarningsCode->getUserId(),
            'nbrearn_data_origin' => $employeeEarningsCode->getDataOrigin(),
            'nbrearn_activity_date' => $employeeEarningsCode->getActivityDate()->format('Y-m-d H:i:s'),
        ]);

        return true;
    }

    /**
     * Only update attributes when user specifies that attributes
     * If not, keep old data
     *
     * @param EmployeeEarningsCode $employeeEarningsCode
     * @return bool
     * @throws \Exception
     */
    public function update(
        EmployeeEarningsCode $employeeEarningsCode
    ): bool {
        $employeeId = $employeeEarningsCode->getMuid();
        $muid = $this->muidRepository->readMUID($employeeId);
        $employeePidm = $muid['pidm'];

        if (empty($employeePidm)) {
            throw new \Exception("Cannot find employee pidm for muid '$employeeId'.");
        }

        $modifiedAttributes = $employeeEarningsCode->getModifiedAttributes();

        $newModel = EmployeeEarningsCodeEloquentModel::firstOrNew(
            [
                'nbrearn_pidm' => $muid['pidm'],
                'nbrearn_posn' => $employeeEarningsCode->getPositionNumber(),
                'nbrearn_suff' => $employeeEarningsCode->getPositionSuffix(),
                'nbrearn_effective_date' => $employeeEarningsCode->getEffectiveDate()->format('Y-m-d H:i:s'),
                'nbrearn_active_ind' => $employeeEarningsCode->getActive(),
                'nbrearn_earn_code' => $employeeEarningsCode->getEarnCode(),
            ]
        );

        if (isset($modifiedAttributes['shift'])) {
            $newModel->nbrearn_shift = $employeeEarningsCode->getShift();
        }

        if (isset($modifiedAttributes['specialRate'])) {
            $newModel->nbrearn_special_rate = $employeeEarningsCode->getSpecialRate();
        }

        if (isset($modifiedAttributes['hours'])) {
            $newModel->nbrearn_hrs = $employeeEarningsCode->getHours();
        }

        if (isset($modifiedAttributes['deemedHours'])) {
            $newModel->nbrearn_deemed_hrs = $employeeEarningsCode->getDeemedHours();
        }

        $newModel->nbrearn_activity_date = $employeeEarningsCode->getActivityDate()->format('Y-m-d H:i:s');
        $newModel->nbrearn_user_id = $employeeEarningsCode->getUserId();
        $newModel->nbrearn_data_origin = $employeeEarningsCode->getDataOrigin();

        $newModel->save();

        return true;
    }

    /**
     * @param string $muid
     * @param string $positionNumber
     * @param string $positionSuffix
     * @param string $earnCode
     * @param string $effectiveDate
     * @param string $active
     * @return array
     */
    public function read(
        string $muid,
        string $positionNumber = '',
        string $positionSuffix = '',
        string $effectiveDate = '',
        string $earnCode = '',
        string $active = ''
    ): array {
        $muid = $this->muidRepository->readMUID($muid);

        $models = EmployeeEarningsCodeEloquentModel
            ::buildSelect()
            ->wherePidm($muid['pidm'])
            ->wherePositionNumber($positionNumber)
            ->wherePositionSuffix($positionSuffix)
            ->whereEarnCode($earnCode)
            ->whereActive($active)
            ->whereSpecificEffectiveDate($effectiveDate)
           ->get();



        if (empty($models)) {
            return [];
        }

        return $this->dataTransformer->transformData($models, ['muid' => $muid]);
    }
}
