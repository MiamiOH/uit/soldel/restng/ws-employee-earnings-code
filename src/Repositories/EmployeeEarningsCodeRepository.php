<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/5/18
 * Time: 2:32 PM
 */

namespace MiamiOH\WSEmployeeEarningsCode\Repositories;

use MiamiOH\WSEmployeeEarningsCode\Objects\EmployeeEarningsCode;

interface EmployeeEarningsCodeRepository
{
    public function create(EmployeeEarningsCode $employeeEarningsCode): bool;

    public function update(EmployeeEarningsCode $employeeEarningsCode): bool;

    public function read(
        string $muid,
        string $positionNumber,
        string $positionSuffix,
        string $effectiveDate,
        string $earnCode,
        string $active
    ): array;
}
