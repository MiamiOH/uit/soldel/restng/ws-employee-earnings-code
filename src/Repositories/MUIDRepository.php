<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 1/18/19
 * Time: 9:17 AM
 */

namespace MiamiOH\WSEmployeeEarningsCode\Repositories;

interface MUIDRepository
{
    public function readMUIDs(array $muids): array;

    public function readMUID(string $muid): array;
}
