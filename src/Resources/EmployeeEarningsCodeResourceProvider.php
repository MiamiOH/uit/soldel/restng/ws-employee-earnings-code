<?php

namespace MiamiOH\WSEmployeeEarningsCode\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;
use MiamiOH\WSEmployeeEarningsCode\Services\EmployeeEarningsCode\Service;

class EmployeeEarningsCodeResourceProvider extends ResourceProvider
{
    private $name = 'employeeEarningsCode';
    private $application = 'WebServices';
    private $module = 'Employee';

    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => $this->name . '.post.put.model',
            'type' => 'object',
            'properties' => [
                // required fields
                'active' => ['type' => 'string', 'enum' => ['required|Y|N']],
                'earnCode' => ['type' => 'string', 'enum' => ['required|string']],
                'effectiveDate' => ['type' => 'string', 'enum' => ['required|date']],
                'muid' => ['type' => 'string', 'enum' => ['required|string']],
                'positionNumber' => ['type' => 'string', 'enum' => ['required|string']],
                'positionSuffix' => ['type' => 'string', 'enum' => ['required|string']],
                'shift' => ['type' => 'string', 'enum' => ['required|string']],

                // non required fields
                'deemedHours' => ['type' => 'string'],
                'hours' => ['type' => 'string'],
                'specialRate' => ['type' => 'string'],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.get.model',
            'type' => 'object',
            'properties' => [
                'muid' => [
                    'type' => 'object',
                    '$ref' => '#/definitions/muid.model',
                ],
                'positionNumber' => [
                    'type' => 'string',
                ],
                'positionSuffix' => [
                    'type' => 'string',
                ],
                'effectiveDate' => [
                    'type' => 'string',
                ],
                'specialRate' => [
                    'type' => 'string',
                ],
                'deemedHours' => [
                    'type' => 'string',
                ],
                'hours' => [
                    'type' => 'string',
                ],
                'shift' => [
                    'type' => 'string',
                ],
                'earnCode' => [
                    'type' => 'string',
                ],
                'active' => [
                    'type' => 'string',
                ]
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->name,
            'class' => Service::class,
            'description' => 'Provide database source',
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'create',
            'description' => 'Create new employee earnings code',
            'name' => $this->name . '.post.single',
            'service' => $this->name,
            'method' => 'postSingle',
            'tags' => ['employee'],
            'pattern' => '/employee/earningsCode/v1',
            'body' => [
                'required' => true,
                'description' => 'earnings code data',
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.post.put.model'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['create', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'read',
            'description' => 'Get new employee earnings code',
            'name' => $this->name . '.get.single',
            'service' => $this->name,
            'method' => 'getSingle',
            'tags' => ['employee'],
            'pattern' => '/employee/earningsCode/v1',
            'isPageable' => true,
            'defaultPageLimit' => 100,
            'maxPageLimit' => 500,
            'options' => [
                'muid' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'pidm, banner id, unique id'
                ],
                'positionNumber' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'position number'
                ],
                'positionSuffix' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'position suffix'
                ],
                'effectiveDate' => [
                    'type' => 'single',
                    'description' => 'effective date format:dd-mm-yyyy or yyyy-mm-dd'
                ],
                'earnCode' => [
                    'type' => 'single',
                    'description' => "earn code"
                ],
                'active' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => "active indicator Y or N"
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A employee earnings code record',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/' . $this->name . '.get.model',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['view', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'update',
            'description' => 'Update employee earnings code',
            'name' => $this->name . '.put.single',
            'service' => $this->name,
            'method' => 'putSingle',
            'tags' => ['employee'],
            'pattern' => '/employee/earningsCode/v1',
            'body' => [
                'required' => true,
                'description' => 'earnings codes data',
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.post.put.model'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Update successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Update operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['update', 'edit', 'all']
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {
    }
}
