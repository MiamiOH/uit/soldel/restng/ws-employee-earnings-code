<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/1/18
 * Time: 8:39 PM
 */

namespace MiamiOH\WSEmployeeEarningsCode\Objects;

use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

/**
 * Class EmployeeEarningsCode
 * @package MiamiOH\WSEmployeeEarningsCode\Objects
 */
class EmployeeEarningsCode
{
    /**
     * @var \DateTime
     */
    private $effectiveDate = null;

    /**
     * @var string
     */
    private $specialRate = '';

    /**
     * @var string
     */
    private $deemedHours = '';

    /**
     * @var string
     */
    private $hours = '';

    /**
     * @var string
     */
    private $muid = '';

    /**
     * @var string
     */
    private $shift = '';

    /**
     * @var string
     */
    private $positionSuffix = '';

    /**
     * @var string
     */
    private $earnCode = '';

    /**
     * @var string
     */
    private $userId = '';

    /**
     * @var string
     */
    private $dataOrigin = '';

    /**
     * @var string
     */
    private $positionNumber = '';

    /**
     * @var string
     */
    private $active = '';

    /**
     * @return \DateTime
     */
    public function getEffectiveDate(): \DateTime
    {
        return $this->effectiveDate;
    }

    /**
     * @param string $effectiveDate
     * @return EmployeeEarningsCode
     */
    public function setEffectiveDate(string $effectiveDate): EmployeeEarningsCode
    {
        $this->effectiveDate = new \DateTime($effectiveDate);
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecialRate(): string
    {
        return $this->specialRate;
    }

    /**
     * @param string $specialRate
     * @return EmployeeEarningsCode
     */
    public function setSpecialRate(string $specialRate): EmployeeEarningsCode
    {
        $this->specialRate = $specialRate;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeemedHours(): string
    {
        return $this->deemedHours;
    }

    /**
     * @param string $deemedHours
     * @return EmployeeEarningsCode
     */
    public function setDeemedHours(string $deemedHours): EmployeeEarningsCode
    {
        $this->deemedHours = $deemedHours;
        return $this;
    }

    /**
     * @return string
     */
    public function getHours(): string
    {
        return $this->hours;
    }

    /**
     * @param string $hours
     * @return EmployeeEarningsCode
     */
    public function setHours(string $hours): EmployeeEarningsCode
    {
        $this->hours = $hours;
        return $this;
    }

    /**
     * @return string
     */
    public function getMuid(): string
    {
        return $this->muid;
    }

    /**
     * @param string $muid
     * @return EmployeeEarningsCode
     */
    public function setMuid(string $muid): EmployeeEarningsCode
    {
        $this->muid = $muid;
        return $this;
    }

    /**
     * @return string
     */
    public function getShift(): string
    {
        return $this->shift;
    }

    /**
     * @param string $shift
     * @return EmployeeEarningsCode
     */
    public function setShift(string $shift): EmployeeEarningsCode
    {
        $this->shift = $shift;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionSuffix(): string
    {
        return $this->positionSuffix;
    }

    /**
     * @param string $positionSuffix
     * @return EmployeeEarningsCode
     */
    public function setPositionSuffix(string $positionSuffix): EmployeeEarningsCode
    {
        $this->positionSuffix = $positionSuffix;
        return $this;
    }

    /**
     * @return string
     */
    public function getEarnCode(): string
    {
        return $this->earnCode;
    }

    /**
     * @param string $earnCode
     * @return EmployeeEarningsCode
     */
    public function setEarnCode(string $earnCode): EmployeeEarningsCode
    {
        $this->earnCode = $earnCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return EmployeeEarningsCode
     */
    public function setUserId(string $userId): EmployeeEarningsCode
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataOrigin(): string
    {
        return $this->dataOrigin;
    }

    /**
     * @param string $dataOrigin
     * @return EmployeeEarningsCode
     */
    public function setDataOrigin(string $dataOrigin): EmployeeEarningsCode
    {
        $this->dataOrigin = $dataOrigin;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionNumber(): string
    {
        return $this->positionNumber;
    }

    /**
     * @param string $positionNumber
     * @return EmployeeEarningsCode
     */
    public function setPositionNumber(string $positionNumber): EmployeeEarningsCode
    {
        $this->positionNumber = $positionNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getActive(): string
    {
        return $this->active;
    }

    /**
     * @param string $active
     * @return EmployeeEarningsCode
     */
    public function setActive(string $active): EmployeeEarningsCode
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActivityDate(): \DateTime
    {
        return new \DateTime('now');
    }

    /**
     * @var array
     */
    private static $rules = [
        // required fields
        'active' => ['required', 'regex:/^Y|N$/i'],
        'earnCode' => ['required', 'max:3'],
        'effectiveDate' => ['required', 'date'],
        'muid' => ['required'], // delegate validation to ws-muid
        'positionNumber' => ['required', 'regex:/^\d{1,6}$/'],
        'positionSuffix' => ['required', 'regex:/^\d{1,2}$/'],
        'shift' => ['required', 'max:1'],

        // non required fields
        'deemedHours' => ['numeric', 'regex:/^\d{1,4}(\.\d*)?$/'],
        'hours' => ['numeric', 'regex:/^\d{1,4}(\.\d*)?$/'],
        'specialRate' => ['numeric', 'regex:/^\d{1,6}(\.\d*)?$/'],

        // non user-defined field
        'dataOrigin' => ['max:30'],
        'userId' => ['max:30'],
    ];

    private $modifiedAttributes = [];

    /**
     * @return array
     */
    public function getModifiedAttributes(): array
    {
        return $this->modifiedAttributes;
    }

    /**
     * @param string $attributes
     */
    public function addModifiedAttributes(string $attributes): void
    {
        $this->modifiedAttributes[$attributes] = true;
    }

    /**
     * @param array $data
     * @return EmployeeEarningsCode
     * @throws \Exception
     */
    public static function fromArray(array $data): self
    {
        self::validateArray($data);

        $thisInstance = new self();

        try {
            foreach ($data as $key => $val) {
                $thisInstance->{'set' . ucfirst($key)}($val);
                $thisInstance->addModifiedAttributes($key);
            }
        } catch (\Exception $e) {
            throw new \Exception("Invalid key for EmployeeEarningCodes: $key" . $e->getMessage());
        }

        return $thisInstance;
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    public static function validateArray(array $data): void
    {
        $validator = RESTngValidatorFactory::make($data, self::$rules);

        if ($validator->fails()) {
            throw new \Exception(
                'Validation failed. '
                . implode(' ', $validator->errors()->all())
            );
        }
    }
}
