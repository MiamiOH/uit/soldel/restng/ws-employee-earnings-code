<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/1/18
 * Time: 8:39 PM
 */

namespace MiamiOH\WSEmployeeEarningsCode\EloquentModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Yajra\Oci8\Query\OracleBuilder;
use Illuminate\Database\Capsule\Manager as DB;

class EmployeeEarningsCodeEloquentModel extends Model
{

    protected $connection = 'MUWS_GEN_PROD';

    /**
     * @var string $table Table name
     */
    public $table = 'nbrearn';

    /**
     * @var bool $timestamps Do not populate auto-generated date fields
     */
    public $timestamps = false;

    /**
     * @var bool $incrementing Do not increment primary key by default
     */
    public $incrementing = false;

    /**
     * @var string $primaryKey Primary key of table
     */
    protected $primaryKey = 'nbrearn_surrogate_id';

    /**
     * @var array $guarded black list of insertable fields
     */
    protected $guarded = [];

    /**
     * Get a new query builder instance for the connection.
     * https://github.com/yajra/laravel-oci8/issues/73#issuecomment-117131744
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new OracleBuilder($conn, $grammar, $conn->getPostProcessor());
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeBuildSelect(Builder $query): Builder
    {
        return $query->select(
            'nbrearn_earn_code AS earn_code',
            'nbrearn_effective_date AS effective_date',
            'nbrearn_special_rate AS special_rate',
            'nbrearn_deemed_hrs AS deemed_hours',
            'nbrearn_hrs AS hours',
            'nbrearn_active_ind AS active',
            'nbrearn_shift AS shift',
            'nbrearn_suff AS position_suffix',
            'nbrearn_posn AS position_number'
        )
            ->orderBy(
                'nbrearn_effective_date',
                'DESC'
            );
    }

    /**
     * @param Builder $query
     * @param string $pidm
     * @return Builder
     */
    public function scopeWherePidm(Builder $query, string $pidm): Builder
    {
        if (!empty($pidm)) {
            $query->where('nbrearn_pidm', $pidm);
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param string $positionNumber
     * @return Builder
     */
    public function scopeWherePositionNumber(Builder $query, string $positionNumber): Builder
    {
        if (!empty($positionNumber)) {
            $query->where('nbrearn_posn', $positionNumber);
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param string $positionSuffix
     * @return Builder
     */
    public function scopeWherePositionSuffix(Builder $query, string $positionSuffix): Builder
    {
        if (!empty($positionSuffix)) {
            $query->where('nbrearn_suff', $positionSuffix);
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param string $earnCode
     * @return Builder
     */
    public function scopeWhereEarnCode(Builder $query, string $earnCode): Builder
    {
        if (!empty($earnCode)) {
            $query->where('nbrearn_earn_code', $earnCode);
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param string $effectiveDate
     * @return Builder
     */
    public function scopeWhereSpecificEffectiveDate(Builder $query, string $effectiveDate): Builder
    {
        if (!empty($effectiveDate)) {
            $effectiveDate=Carbon::parse($effectiveDate)->format('Ymd');
            $query->where('nbrearn_effective_date','=', $effectiveDate);
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param \DateTime $effectiveDate
     * @return Builder
     */
    public function scopeWhereEffectiveDate(Builder $query, $pidm, $positionNumber, $positionSuffix, $earnCode, $active): Builder
    {
        $query->where(
            'nbrearn_effective_date',
            function ($query) use ($pidm, $positionNumber, $positionSuffix, $earnCode, $active) {
                $query
                    ->select(DB::raw('max("NBREARN_EFFECTIVE_DATE")'))
                    ->from('NBREARN "INNER_NBREARN"')
                    ->where('nbrearn_pidm', $pidm)
                    ->where('nbrearn_posn', $positionNumber)
                    ->where('nbrearn_suff', $positionSuffix)
                    ->where('NBREARN.NBREARN_EARN_CODE', DB::raw('"INNER_NBREARN"."NBREARN_EARN_CODE"'))
                    ->where('nbrearn_active_ind', $active)
                    ->where('nbrearn_effective_date', '<=', DB::raw('SYSDATE'))
                ;
            }
        );

        return $query;
    }

    /**
     * @param Builder $query
     * @param string $active
     * @return Builder
     */
    public function scopeWhereActive(Builder $query, string $active): Builder
    {
        if (!empty($active)) {
            $query->where('nbrearn_active_ind', $active);
        }
        return $query;
    }
}
