<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 1/25/19
 * Time: 11:31 AM
 */

namespace MiamiOH\WSEmployeeEarningsCode\DataTransformers;

use Illuminate\Support\Collection;

class EmployeeEarningsCodeDataTransformer extends DataTransformer
{
    /**
     * @param Collection $collection
     * @param array $attributes
     * @return array
     */
    public function transformData(Collection $collection, array $attributes = []): array
    {
        foreach ($collection as $model) {
            $model->muid = $attributes['muid'];
            $model->effective_date = (new \DateTime($model->effective_date))->format('Y-m-d\TH:i:sP');
        }

        return $this->arrayKeySnakeToCamel($collection->toArray());
    }
}
