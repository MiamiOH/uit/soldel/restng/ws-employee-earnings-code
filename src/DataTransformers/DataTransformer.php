<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 1/22/19
 * Time: 3:21 PM
 */

namespace MiamiOH\WSEmployeeEarningsCode\DataTransformers;

use Illuminate\Support\Collection;

abstract class DataTransformer
{
    /**
     * @param array $array
     * @return array
     */
    public function arrayKeySnakeToCamel(array $array): array
    {
        $camelKeys = [];

        foreach ($array as $key => $element) {
            if (is_array($element)) {
                $element = $this->arrayKeySnakeToCamel($element);
            }

            $camelKeys[$this->stringSnakeToCamel($key)] = $element;
        }

        return $camelKeys;
    }

    /**
     * @param string $s
     * @return string
     */
    public function stringSnakeToCamel(string $s)
    {
        $s = preg_replace_callback('/_([a-z])/', function ($matches) {
            return strtoupper($matches[1]);
        }, $s);

        return lcfirst($s);
    }

    abstract public function transformData(Collection $collection, array $attributes = []);
}
