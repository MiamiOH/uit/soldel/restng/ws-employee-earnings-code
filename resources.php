<?php

return [
    'resources' => [
        'employee' => [
            \MiamiOH\WSEmployeeEarningsCode\Resources\EmployeeEarningsCodeResourceProvider::class
        ],
    ]
];
