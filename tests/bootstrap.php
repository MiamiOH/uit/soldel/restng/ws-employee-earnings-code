<?php

// Add our function mocks to the include path
set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__ . '/../tests/includes');

ini_set("display_errors", "on");
error_reporting(E_ALL | E_STRICT);

require_once(__DIR__ . '/../vendor/autoload.php');

function getMicroTime()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}
