<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 1/22/19
 * Time: 4:12 PM
 */

namespace MiamiOH\WSEmployeeEarningsCode\Tests\Unit;

use MiamiOH\WSEmployeeEarningsCode\DataTransformers\EmployeeEarningsCodeDataTransformer;

class DataTransformerTest extends \MiamiOH\RESTng\Testing\TestCase
{
    public function testTranslateArray()
    {
        $dataTransformer = new EmployeeEarningsCodeDataTransformer();

        $raw = [
            'hello_world' => 'echo_back',
            'nested_array' => [
                'who_are_we' => 'imagine_self',
                'is_reality_real' => 'selfless_chaos'
            ]
        ];

        $expected = [
            'helloWorld' => 'echo_back',
            'nestedArray' => [
                'whoAreWe' => 'imagine_self',
                'isRealityReal' => 'selfless_chaos'
            ]
        ];
        $actual = $dataTransformer->arrayKeySnakeToCamel($raw);

        $this->assertEquals($expected, $actual);
    }
}
